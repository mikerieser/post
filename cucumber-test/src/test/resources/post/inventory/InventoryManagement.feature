@PDR-12345
Feature: Inventory Management
  PRD Statement:
  Inventory Management is all about managing the store catalog and what is says about
  the products we sell, their prices, how many items we have in stock, and
  where in the store I can find those items.
  
  Managing the inventory involves the following activities / scenarios:
  - Add new products to the store catalog with initial values for:
    (upc, description, location, unit price, and quantity of items on hand)
  - Retrieve a list of products currently in the store catalog
  - Update all product attributes associated with a given upc
  - Update quantities of existing items when we receive shipments
  - Remove an item from the store catalog based on upc

  #  Background:
  #    Given I have a new store catalog
  @IM_AddProduct
  Scenario Outline: Add new product to the store catalog
    
    As a Manager,
    I want to add new products to the catalog of items the store sells,
    so that I sell items our customers want.

    Given a store with an empty catalog
    When the Manager adds a product to catalog
      | upc   | product description   | unit price   | quantity on hand   | store location   |
      | <upc> | <product description> | <unit price> | <quantity on hand> | <store location> |
    Then the result of adding the product should be "<expected result>"

    Examples: 
      | expected result     | upc     | product description | unit price | quantity on hand | store location |
      | OK                  |     100 | Water Bottle        | $2.00      |              102 | fridge         |
      | invalid upcCode     | '@#$%!' | Coffee Grinder      | $15.00     |                1 | entry display  |
      | missing description |     900 |                     | $2.00      |              102 | fridge         |
      | invalid quantity    |     900 | Water Bottle        | $2.00      |               -1 | fridge         |
      | invalid unit price  |     900 | Water Bottle        | $0.00      |              102 | fridge         |
      | invalid unit price  |     900 | Water Bottle        | $-1.00     |              102 | fridge         |
      | missing location    |     900 | Water Bottle        | $2.00      |              102 |                |

  @IM_Duplicates
  Scenario: Duplicate items not allowed in store catalog (Option A)
    
    As a Manager,
    I want to prevent duplicate product specifications from being added to the store catalog,
    so that our inventory is accurate.

    Given the store catalog contains
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        |       1.00 |               85 | fridge         |
    When the Manager adds a product to catalog
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        | $1.00      |               85 | fridge         |
    Then it should be "duplicate UPC"

  @IM_Duplicates
  Scenario: Two different products may not have the same UPC code (Option D)
    
    As a Manager,
    I want to make sure duplicate products can not be added to the store catalog,
    so that product mistakes are not made

    Given the store catalog contains "Water Bottle" with upc "100"
    When I add to the store catalog "Lemonade" with upc "100"
    Then it should be "ERROR"

  @IM_Update_Desc
  Scenario: Update product descriptions
    
    As a Manager,
    I want to be able to update product descriptions in the store catalog,
    so that the store catelog is always correct

    Given the store catalog contains
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        |       1.00 |               85 | fridge         |
    When I update product with upc "100" to have the description "Gatorade"
    Then product description for upc "100" should be "Gatorade"

  @IM_Update_Quantities
  Scenario: Update product quantities
    
    As a Manager,
    I want to be able to update quantity on hand for products in the store catalog,
    so that the store catelog accurately reflects how many items we have to sell

    Given the store catalog contains
      | upc | product description | unit price | quantity on hand | store location |
      | 100 | Water Bottle        |       1.00 |                5 | fridge         |
    When we add 50 items of "100" to the catalog
    Then there should be 55 items of "100" in stock

  @tag3
  Scenario Outline: Look up a product with UPC code
    
    As a Store Manager,
    I want to be able to find the quantity on hand for each product in inventory
    So that I know how many we have on hand

    Given the store catalog contains
      | upc | product description   | unit price | quantity on hand | store location |
      | 100 | Water Bottle          |       1.00 |               85 | fridge         |
      | 110 | Diet Coke 12oz Bottle |       1.50 |              195 | fridge         |
      | 112 | Coke 12oz Bottle      |       1.50 |              123 | fridge         |
      | 114 | Chocolate Cup Cakes   |       2.25 |               16 | Shelf C        |
    When I lookup the product with UPC code "<upc>"
    Then the result should be "<outcome>"

    Examples: 
      | scenario    | upc            | outcome           |
      | found       |            100 | product found     |
      | not found   |            999 | product not found |
      | invalid UPC | XXX            | invalid UPC       |
      | invalid UPC | 114s           | invalid UPC       |
      | found       |            112 | product found     |
      | invalid UPC |                | invalid UPC       |
      | Long UPC    | 11111111111111 | product not found |
