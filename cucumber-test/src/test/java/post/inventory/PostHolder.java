package post.inventory;

import post.domain.Post;

public class PostHolder {
	private static Post post;
	
	public static Post getPost() {
		if (post == null) {
			post = new Post();
		}
		return post;
	}

	public static void destroyPost() {
		post = null;
	}
}
