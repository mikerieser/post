package post.inventory;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import post.domain.ProductSpecification;

import static post.inventory.PostHolder.*;

public class InventoryManagement {

	private String upc;
	private String desc;
	private double price;
	private int quantityOnHand;
	private String location;
	private String result;
	private String find_result;

	@When("the Manager adds a product to catalog")
	public void the_manager_adds_a_product_to_catalog(io.cucumber.datatable.DataTable dataTable) {
		List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);

		Map<String, String> columns = rows.get(0);
		String upc = columns.get("upc");
		String desc = columns.get("product description");
		double unitPrice = Double.parseDouble(columns.get("unit price").substring(1));
		int quantityOnHand = Integer.parseInt(columns.get("quantity on hand"));
		String location = columns.get("store location");
		try {
			getPost().addNewProduct(upc, desc, unitPrice, quantityOnHand, location);
			result = "OK";
		} catch (Exception e) {
			result = e.getMessage();
		}
	}

	@After
	public void tearDownPost() {
		destroyPost();
	}

	@Given("a store with an empty catalog")
	public void i_have_a_new_store_catalog() {
		destroyPost();
	}

	@Given("the store catalog contains")
	public void the_following_store_catalog_by_map(io.cucumber.datatable.DataTable dataTable) {

		List<Map<String, String>> rows = dataTable.asMaps(String.class, String.class);

		for (Map<String, String> columns : rows) {
			getPost().addNewProduct(columns.get("upc"), columns.get("product description"),
					Double.parseDouble(columns.get("unit price")), Integer.parseInt(columns.get("quantity on hand")),
					columns.get("store location"));
		}
	}

	@Given("the store catalog contains {string} with upc {string}")
	public void the_store_catalog_contains_with_upc(String desc, String upc) {

		this.upc = upc;
		this.desc = desc;
		// Remaining product attributes are default values
		this.price = 1.29;
		this.quantityOnHand = 33;
		this.location = "Shelf A";
		try {
			getPost().addNewProduct(this.upc, this.desc, this.price, this.quantityOnHand, this.location);
			result = "OK";
		} catch (Exception e) {
			result = "ERROR";
		}
	}

	@When("I add to the store catalog {string} with upc {string}")
	public void i_add_to_the_store_catalog_with_upc(String desc, String upc) {
		this.upc = upc;
		this.desc = desc;
		// Remaining product attributes are default values
		this.price = 1.59;
		this.quantityOnHand = 23;
		this.location = "Shelf B";
		try {
			getPost().addNewProduct(this.upc, this.desc, this.price, this.quantityOnHand, this.location);
			result = "OK";
		} catch (Exception e) {
			result = "ERROR";
		}
	}

	@When("I update product with upc {string} to have the description {string}")
	public void i_update_the_description_for_product_upc_to_be(String upc, String newDescription) {

		ProductSpecification product = getPost().lookupItem(upc);
		product.updateDescription(newDescription);
	}

	@Then("it should be {string}")
	public void it_should_be(String expected) {
		assertEquals(expected, result);

	}

	@Then("product description for upc {string} should be {string}")
	public void product_description_for_upc_should_be(String upc, String desc) {
		ProductSpecification product = getPost().lookupItem(upc);
		assertEquals(desc, product.getDescription());
	}

	@When("we add {int} items of {string} to the catalog")
	public void we_add_items_of_to_the_catalog(Integer additionalQuantity, String upc) {
		getPost().updateQuantity(upc, additionalQuantity);
	}

	@Then("there should be {int} items of {string} in stock")
	public void there_should_be_items_of_in_stock(Integer finalQuantity, String upc) {
		ProductSpecification product = getPost().lookupItem(upc);
		assertEquals(finalQuantity, product.getQuantity());
	}

	@When("I lookup the product with UPC code {string}")
	public void i_lookup_the_product_with_upc_code(String upc) {

		try {
			ProductSpecification product = getPost().lookupItem(upc);
			if (product == null)
				find_result = "product not found";
			else
				find_result = "product found";
		} catch (Exception e) {
			find_result = "invalid UPC";
		}

	}

	@Then("the result should be {string}")
	public void the_product_should_be(String outcome) {
		assertEquals(outcome, find_result);
	}

	@Then("the result of adding the product should be {string}")
	public void the_product_add_result_should_be(String outcome) {
		assertEquals(outcome, result);
	}

}
