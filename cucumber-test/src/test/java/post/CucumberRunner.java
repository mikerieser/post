package post;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber-report.html", "json:target/cucumber-report.json" } //
		, features = { "src/test/resources" } //
		, monochrome = true //
//		, glue = { "post.inventory" } // default: starts looking in the Runner's package and descends below it.
		, tags = "not @WIP"
) // 
public class CucumberRunner {

}