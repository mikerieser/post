package post.domain;

public class Payment {
	private Double paymentAmount;
	private Sale sale;

	public Payment(Sale sale, Double paymentAmount) {
		this.sale = sale;
		this.paymentAmount = paymentAmount;
	}

	public double getChangeDue() {
		return paymentAmount - sale.getTotal();
	}
}
