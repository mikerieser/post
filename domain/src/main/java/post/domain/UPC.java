package post.domain;

public class UPC {
	private String upcCode;
	
	public UPC(String upc) {
		if (upc == null || upc.equals("") || !upc.matches("[0-9]+"))
			throw new IllegalArgumentException("invalid upcCode");

		this.upcCode = upc;
	}
	
	@Override
	public int hashCode() {
		return upcCode.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof UPC))
			return false;
		UPC other = (UPC) obj;
		return upcCode.equals(other.upcCode);
	}
	
	@Override
	public String toString() {
		return upcCode;
	}

	public String getUpcCode() {
		return upcCode;
	}
	
}