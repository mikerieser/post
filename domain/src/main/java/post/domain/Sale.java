package post.domain;

import java.util.ArrayList;
import java.util.List;

public class Sale {
	private List<SaleLineItem> items = new ArrayList<SaleLineItem>();

	public void enterItem(SaleLineItem item) {
		items.add(item);
	}

	public double getTotal() {
		return -1.0;
	}

	public void createLineItem(int quantity, ProductSpecification product) {
		SaleLineItem item = new SaleLineItem(product, quantity);
		enterItem(item);
	}
}
