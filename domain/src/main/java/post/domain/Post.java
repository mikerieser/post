package post.domain;

public class Post {	
	private Catalog catalog = new Catalog();
	private Sale sale;

	/* Customer Checkout methods */
	
	public void enterItem(String upc, int quantity) {
		if (sale == null) {
			sale = new Sale();
		}
		ProductSpecification product = catalog.lookup(upc);
		sale.createLineItem(quantity, product);
	}

	public double getTotal() {
		return sale.getTotal();
	}

	/* Inventory Management methods */
	
	public void addNewProduct(String upc, String desc, double price, int quantityOnHand, String location) {
		catalog.addNewProduct(upc, desc, price, quantityOnHand, location);
	}

	public ProductSpecification lookupItem(String upcCode) {
		return catalog.lookup(upcCode);
	}

	public void updateQuantity(String upc, int additionalQuantity) {
		catalog.updateQuantity(upc, additionalQuantity);
	}

	public void updateDescription(String upc, String newDescription) {
		catalog.updateDescription(upc, newDescription);
	}

	public Sale endSale() {
		return sale;
	}

	public Payment makePayment(Double amountTendered) {
		return new Payment(sale, amountTendered);
	}

}
