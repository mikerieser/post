package post.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

//import post.domain.Post;
//import post.domain.ProductSpecification;

public class UpdateInventoryTest {
	private Post post = new Post();

	@Before
	public void setup() {
		post.addNewProduct("100", "Water Bottle", 2.00, 100, "fridge");
	}

	@Test
	public void updateProductShouldIncrementQuantityOnHand() {
		post.updateQuantity("100", 10);
		ProductSpecification product = post.lookupItem("100");

		assertEquals(110, product.getQuantity());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldNotAllowInvalidUpc() {
		post.updateQuantity("xxx", 10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void tooMuchReductionShouldThrowError() {
		post.updateQuantity("100", -200);
	}

	@Test
	public void updateProductShouldCanDecrementQuantityOnHand() {
		post.updateQuantity("100", -4);
		ProductSpecification product = post.lookupItem("100");

		assertEquals(96, product.getQuantity());
	}
}
