package post.domain;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

//import post.domain.Post;

public class PostTest {
	private Post post = new Post();

	@Before
	public void setup() {
		post.addNewProduct("0100", "Water Bottle", 1.00, 50, "Fridge");
		post.addNewProduct("0200", "Granola Bar", 4.00, 85, "Shelf A");
	}

	@Ignore
	@Test
	public void totalShouldReturnSumTotalOfAllItems() {
		post.enterItem("0100", 2);
		post.enterItem("0200", 1);

		assertEquals(6.00, post.getTotal(), 0.005);
	}

	@Ignore
	@Test
	public void simpleSaleForOneItemReturnsUnitPriceForItem() {
		post.enterItem("0100", 1);
		Sale sale = post.endSale();

		assertEquals(1.00, sale.getTotal(), 0.005);
	}

	@Ignore
	@Test
	public void paymentShouldBalanceSale() {
		post.enterItem("0100", 2);
		post.enterItem("0200", 1);
		post.endSale();
		Payment payment = post.makePayment(6.00);

		assertEquals(0.00, payment.getChangeDue(), 0.005);
	}
}
