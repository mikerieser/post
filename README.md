# Getting Started 
* Click 'Clone' above. 
* Select 'Https' unless you already have 'ssh' setup with bitbucket.
* Click 'Copy to Clipboard'
* In a command shell on your computer, navigate to wherever you want the project.
* Paste the copied git command. e.g.
```
git clone https://mikerieser@bitbucket.org/mikerieser/post.git
```
* The project should build and pass all it's tests using the following:
```
gradlew build test
```

## IDE Plugins
Tools like Eclipse and Idea can be extended with plugins to make working with Cucumber better.

### Eclipse
* Execute the following gradle command to create Eclipse projects.
```
gradlew eclipse
```
* Start Eclipse and provide it workspace to use. Do not use the post directory.
* Install the Cucumber Eclipse Plugin: 
* Help -> Eclipse Marketplace ... 
* Type 'Cucumber' into the search box. 
* Select the plugin 'Cucumber Eclipse Plugin' by 'Cucumber'
* File -> Import -> Existing projects into workspace
* Use the 'Select Root directory' option.
* Click 'Browse' and navigate to where the git repo was cloned to.
* Click 'Finish'.
* Right-Click on the 'cucumber-test' project.
    * Select 'Configure' -> 'Convert to Cucumber Project'
* Both projects should allow for running JUnit tests.

### Idea
* Cucumber for Java
* Gherkin (you will be asked to include this one when you mention the other)

To install these plugins:

* File : Settings (windows: ctrl - alt - s, mac: command - ,)
* Navigate to: Plugins
* Search for "Cucumber for Java"
* Install it, and when prompted, also include the Gherkin plugin
* Restart Idea

